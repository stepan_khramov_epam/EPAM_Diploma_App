output "vpc_zones" {
  value = data.aws_availability_zones.names
}

output "eks_endpoint" {
  value = aws_eks_cluster.diploma_eks_cl_01.endpoint
}

resource "local_file" "eks_alb_manifest" {
  content = templatefile("eks_alb_manifest.tmplt",
    {
      eks_cl01_name = aws_eks_cluster.diploma_eks_cl_01.name
    }
  )
  filename = "../EKS_mans/eks_alb_manifest.yml"
}

resource "local_file" "cr_gitlab_manifest" {
  content = templatefile("cr_gitlab_manifest.tmplt",
    {
      # gitlab_fqdn = aws_lb.diploma_lb.dns_name
    }
  )
  filename = "../EKS_mans/cr_gitlab_manifest.yml"
}

resource "null_resource" "update_kubectl_config" {
  provisioner "local-exec" {
    command = "aws eks --region ${var.aws_region} update-kubeconfig --name ${var.eks_cl01_name} --profile darhar_mfa"
  }
  depends_on = [aws_eks_cluster.diploma_eks_cl_01, aws_eks_node_group.diploma_eks_eksng_01]
}

resource "null_resource" "create_iam_oidc_provider" {
  provisioner "local-exec" {
    command = "sleep 30 && eksctl utils associate-iam-oidc-provider --region ${var.aws_region} --cluster ${var.eks_cl01_name} --approve --profile darhar_mfa"
  }
  depends_on = [null_resource.update_kubectl_config, local_file.cr_gitlab_manifest, ]
}

resource "null_resource" "delete_iamserviceaccount" {
  provisioner "local-exec" {
    command = "sleep 30 && eksctl delete iamserviceaccount --cluster=${var.eks_cl01_name} --namespace=kube-system --name=aws-load-balancer-controller --profile darhar_mfa"
  }
  depends_on = [null_resource.create_iam_oidc_provider, ]
}

resource "null_resource" "create_iamserviceaccount" {
  provisioner "local-exec" {
    command = "sleep 30 && eksctl create iamserviceaccount --cluster=${var.eks_cl01_name} --namespace=kube-system --name=aws-load-balancer-controller --attach-policy-arn=arn:aws:iam::767874962493:policy/AWSLoadBalancerControllerIAMPolicy --override-existing-serviceaccounts --region ${var.aws_region} --approve --profile darhar_mfa"
  }
  depends_on = [null_resource.delete_iamserviceaccount, ]
}

resource "null_resource" "deploy_cert_manager" {
  provisioner "local-exec" {
    command = "sleep 30 && kubectl apply -f ../EKS_mans/cert_mngr_manifest.yml"
  }
  depends_on = [null_resource.create_iamserviceaccount, ]
}

resource "null_resource" "deploy_aws_alb" {
  provisioner "local-exec" {
    command = "sleep 30 && kubectl apply -f ../EKS_mans/eks_alb_manifest.yml"
  }
  depends_on = [null_resource.deploy_cert_manager, ]
}

resource "null_resource" "deploy_gitlab_manifes" {
  provisioner "local-exec" {
    command = "sleep 30 && kubectl apply -f ../EKS_mans/gitlab_k8s_manifest.yml"
  }
  depends_on = [null_resource.deploy_aws_alb, ]
}