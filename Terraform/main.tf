#==================================================================================================================================================================================================================
#==================================================================================================================================================================================================================
#========== Privat account. =================================================================================================================================================================================
#==================================================================================================================================================================================================================
#==================================================================================================================================================================================================================


# ========== Provider ==============================================
# ==================================================================
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "darhar_mfa"
  region = var.aws_region
}

# ========== IAM ===============================================
# ==================================================================

# data "aws_iam_role" "ec2role" {
#   name = "EC2Role"
# }

data "aws_iam_role" "eks_role" {
  name = "AmazonEKSCluster"
}

data "aws_iam_role" "eks_ng_role" {
  name = "AWSServiceRoleForAmazonEKSNodegroup"
}

data "aws_iam_role" "eks_svc_role" {
  name = "AWSServiceRoleForAmazonEKS"
}

# ========== VPC ===============================================
# ==================================================================

data "aws_vpc" "default" {
  default = true
}

data "aws_region" "current" {}

data "aws_availability_zones" "names" {}

# ========== Subnets ===============================================
# ==================================================================
data "aws_subnet" "int-subnet-01" {
  filter {
    name   = "subnet-id"
    values = ["subnet-0ce856f877576de66"]
  }
}

data "aws_subnet" "int-subnet-02" {
  filter {
    name   = "subnet-id"
    values = ["subnet-0515cf8d990d34dbf"]
  }
}

data "aws_subnet" "int-subnet-03" {
  filter {
    name   = "subnet-id"
    values = ["subnet-07d1afac662ffe263"]
  }
}

resource "aws_db_subnet_group" "diploma_db_subnet_group" {
  subnet_ids = [data.aws_subnet.int-subnet-01.id, data.aws_subnet.int-subnet-02.id, data.aws_subnet.int-subnet-03.id]

  tags = {
    owner = var.owner
  }
}

# ========== ALB ===================================================
# ==================================================================
# resource "aws_lb" "diploma_lb" {
#   name               = "diploma-lb"
#   internal           = false
#   load_balancer_type = "application"
#   security_groups    = [data.aws_security_group.diploma_lb_sg.id]
#   subnets            = [data.aws_subnet.int-subnet-01.id, data.aws_subnet.int-subnet-02.id, data.aws_subnet.int-subnet-03.id]

#   enable_deletion_protection = false

#   # access_logs {
#   #   bucket  = aws_s3_bucket.lb_logs.bucket
#   #   prefix  = "test-lb"
#   #   enabled = true
#   # }

#   tags = {
#     Name  = "KhramovSA_Diploma_lb"
#     owner = var.owner
#   }
# }

# resource "aws_lb_target_group" "diploma_lb_tg" {
#   vpc_id = data.aws_vpc.default.id
#   name     = "diploma-lb-tg"
#   port     = 80
#   protocol = "HTTP"

#   health_check {
#     port     = 80
#     protocol = "HTTP"
#     timeout  = 5
#     interval = 10
#   }
# }

# ========== Not used for now. Begining ==========
# resource "aws_lb_target_group_attachment" "diploma_lb_tg_attchmnt_inst_01" {
#   target_group_arn = aws_lb_target_group.diploma_lb_tg.arn
#   target_id        = aws_instance.diploma_k8s_node-01.id
#   port             = 80
# }

# resource "aws_lb_target_group_attachment" "diploma_lb_tg_attchmnt_inst_02" {
#   target_group_arn = aws_lb_target_group.diploma_lb_tg.arn
#   target_id        = aws_instance.diploma_k8s_node-02.id
#   port             = 80
# }

# resource "aws_lb_target_group_attachment" "diploma_lb_tg_attchmnt_inst_03" {
#   count            = 3
#   target_group_arn = aws_lb_target_group.diploma_lb_tg.arn
#   target_id        = aws_instance.diploma_k8s_node-03.id
#   port             = 80
# }

# resource "aws_alb_listener" "diploma_lb_listener" {  
#   load_balancer_arn = aws_lb.diploma_lb.arn
#   port              = "80"
#   protocol          = "HTTP"
  
#   default_action {    
#     target_group_arn = aws_lb_target_group.diploma_lb_tg.arn
#     type             = "forward"  
#   }
# }


# ========== ELB ===================================================
# ==================================================================
# resource "aws_elb" "diploma_lb" {
#   name            = "diploma-lb"
#   internal        = false
#   subnets         = [data.aws_subnet.int-subnet-01.id, data.aws_subnet.int-subnet-02.id, data.aws_subnet.int-subnet-03.id]
#   security_groups = [data.aws_security_group.diploma_lb_sg.id]

#   listener {
#     instance_port     = 80
#     instance_protocol = "http"
#     lb_port           = 80
#     lb_protocol       = "http"
#   }

#   health_check {
#     healthy_threshold   = 5
#     unhealthy_threshold = 2
#     timeout             = 5
#     target              = "HTTP:80/readme.html"
#     interval            = 10

#   }

#   instances                   = [aws_instance.diploma_k8s_node-01.id, aws_instance.diploma_k8s_node-02.id, aws_instance.diploma_k8s_node-03.id]
#   cross_zone_load_balancing   = false
#   connection_draining         = true
#   connection_draining_timeout = 120

#   tags = {
#     Name  = "KhramovSA_Diploma_lb"
#     owner = var.owner
#   }
# }
# ========== Not used for now. End. ==========

# ========== LB security group ====================================
# ==================================================================
# data "aws_security_group" "diploma_lb_sg" {
#   filter {
#     name   = "group-name"
#     values = ["Allow_conn_from_my_ip"]
#   }
# }

# ========== EFS ===================================================
# ==================================================================
# resource "aws_efs_file_system" "efs_for_dip" {

#   tags = {
#     Name  = "KhramovSA_Diploma_efs_for_dip"
#     owner = var.owner
#   }
# }

# resource "aws_efs_mount_target" "efs_mount_target_diploma_inst-01" {
#   file_system_id  = aws_efs_file_system.efs_for_dip.id
#   subnet_id       = data.aws_subnet.int-subnet-01.id
#   security_groups = [data.aws_security_group.diploma_efs_sg.id]
# }

# resource "aws_efs_mount_target" "efs_mount_target_diploma_inst-02" {
#   file_system_id  = aws_efs_file_system.efs_for_dip.id
#   subnet_id       = data.aws_subnet.int-subnet-02.id
#   security_groups = [data.aws_security_group.diploma_efs_sg.id]
# }

# resource "aws_efs_mount_target" "efs_mount_target_diploma_inst-03" {
#   file_system_id  = aws_efs_file_system.efs_for_dip.id
#   subnet_id       = data.aws_subnet.int-subnet-03.id
#   security_groups = [data.aws_security_group.diploma_efs_sg.id]
# }

# # ========== EFS security group ==============================
# # ==================================================================
# data "aws_security_group" "diploma_efs_sg" {
#   tags = {
#     "EPAM Cloud Default SG" = "true"
#     "or2-created-by"        = "orchestrator"
#   }
#   filter {
#     name   = "group-name"
#     values = ["Allow_conn_from_my_ip"]
#   }
# }

# # ========== DB instance ===========================================
# # ==================================================================
# resource "aws_db_instance" "diploma_db" {
#   identifier             = "diploma-db"
#   engine                 = "mysql"
#   engine_version         = "5.7.34"
#   allocated_storage      = 20
#   instance_class         = "db.t2.micro"
#   vpc_security_group_ids = [data.aws_security_group.diploma_db_sg.id]
#   availability_zone      = "eu-central-1c"
#   db_subnet_group_name   = aws_db_subnet_group.diploma_db_subnet_group.id
#   name                   = var.db_name
#   username               = var.db_user
#   password               = var.db_pass
#   parameter_group_name   = "default.mysql5.7"
#   skip_final_snapshot    = true
#   # depends_on             = [aws_efs_file_system.efs_for_dip]

#   tags = {
#     Name  = "KhramovSA_Diploma_db"
#     owner = var.owner
#   }
# }

# # ========== DB security group =====================================
# # ==================================================================
# data "aws_security_group" "diploma_db_sg" {
#   tags = {
#     "EPAM Cloud Default SG" = "true"
#     "or2-created-by"        = "orchestrator"
#   }
#   filter {
#     name   = "group-name"
#     values = ["Allow_conn_from_my_ip"]
#   }
# }


# ========== EKS ===================================================
# ==================================================================
resource "aws_eks_cluster" "diploma_eks_cl_01" {
  name     = var.eks_cl01_name
  role_arn = data.aws_iam_role.eks_role.arn

  vpc_config {
    subnet_ids = [data.aws_subnet.int-subnet-01.id, data.aws_subnet.int-subnet-02.id, data.aws_subnet.int-subnet-03.id]
    endpoint_public_access = true
    security_group_ids = [data.aws_security_group.diploma_eks_master_sg.id]
  }

  tags = {
    Name  = var.eks_cl01_name
    owner = var.owner
  }

  version = "1.21"

}

resource "aws_eks_node_group" "diploma_eks_eksng_01" {
  cluster_name    = aws_eks_cluster.diploma_eks_cl_01.name
  node_group_name = var.eks_eksng01_name
  node_role_arn   = data.aws_iam_role.eks_role.arn
  subnet_ids      = [data.aws_subnet.int-subnet-01.id, data.aws_subnet.int-subnet-02.id, data.aws_subnet.int-subnet-03.id]

  instance_types = ["t2.large"]
  capacity_type = "ON_DEMAND"
  scaling_config {
    desired_size = 2
    max_size     = 3
    min_size     = 1
  }
  
  update_config {
    max_unavailable = 1
  }

  lifecycle {
    ignore_changes = [scaling_config[0].desired_size]
  }

  tags = {
    Name  = var.eks_eksng01_name
    owner = var.owner
  }

  version = "1.21"

}

# ========== EKS security group =====================================
# ==================================================================
data "aws_security_group" "diploma_eks_master_sg" {
  filter {
    name   = "group-name"
    values = ["Allow_conn_from_my_ip"]
  }
}

resource "aws_iam_openid_connect_provider" "default" {
  url = "https://accounts.google.com"

  client_id_list = [
    "266362248691-342342xasdasdasda-apps.googleusercontent.com",
  ]

  thumbprint_list = []
}


# ========== Instances =============================================
# ==================================================================

# data "template_file" "init_cfg" {
#   template = file("./cloud-init.yaml")
#   vars = {
#     "aws_efs_dns_name"     = "${aws_efs_file_system.efs_for_dip.dns_name}"
#     "ansible_user_pass"    = "${var.ansible_user_pass}"
#     "root_pass"            = "${var.root_pass}"
#     "db_name"              = "${var.db_name}"
#     "db_user"              = "${var.db_user}"
#     "db_password"          = "${var.db_pass}"
#     "db_host"              = "${aws_db_instance.diploma_db.address}"
#     "my_ctrl_node_pub_key" = "${var.my_ctrl_node_pub_key}"
#     "my_virt_pc_pub_key"   = "${var.my_virt_pc_pub_key}"
#   }
# }

# data "template_cloudinit_config" "cloudinit_cfg" {
#   gzip          = false
#   base64_encode = false

#   part {
#     filename     = "./cloud-init.yaml"
#     content_type = "text/cloud-config"
#     content      = data.template_file.init_cfg.rendered
#   }
# }

# resource "aws_instance" "diploma_k8s_node-01" {
#   ami                         = "ami-06ec8443c2a35b0ba"
#   instance_type               = "t2.micro"
#   vpc_security_group_ids      = [data.aws_security_group.diploma_sg_epam_ip.id]
#   iam_instance_profile        = data.aws_iam_instance_profile.EC2Role_profile.name
#   subnet_id                   = data.aws_subnet.int-subnet-01.id
#   associate_public_ip_address = true
#   key_name                    = "KhramovSA-SSH"
#   depends_on                  = [aws_db_instance.diploma_db]

#   user_data = data.template_cloudinit_config.cloudinit_cfg.rendered

#   # root_block_device {
#   #   volume_size = 20
#   # }

#   tags = {
#     Name  = "KhramovSA_Diploma_k8s_node-01"
#     owner = var.owner
#   }
#   volume_tags = {
#     Name  = "KhramovSA_Diploma_k8s_node-01"
#     owner = var.owner
#   }
# }

# resource "aws_instance" "diploma_k8s_node-02" {
#   ami                         = "ami-06ec8443c2a35b0ba"
#   instance_type               = "t2.micro"
#   vpc_security_group_ids      = [data.aws_security_group.diploma_sg_epam_ip.id]
#   iam_instance_profile        = data.aws_iam_instance_profile.EC2Role_profile.name
#   subnet_id                   = data.aws_subnet.int-subnet-02.id
#   associate_public_ip_address = true
#   key_name                    = "KhramovSA-SSH"
#   depends_on                  = [aws_db_instance.diploma_db]

#   user_data = data.template_cloudinit_config.cloudinit_cfg.rendered

#   # root_block_device {
#   #   volume_size = 20
#   # }

#   tags = {
#     Name  = "KhramovSA_Diploma_k8s_node-02"
#     owner = var.owner
#   }
#   volume_tags = {
#     Name  = "KhramovSA_Diploma_k8s_node-02"
#     owner = var.owner
#   }
# }

# resource "aws_instance" "diploma_k8s_node-03" {
#   ami                         = "ami-06ec8443c2a35b0ba"
#   instance_type               = "t2.micro"
#   vpc_security_group_ids      = [data.aws_security_group.diploma_sg_epam_ip.id]
#   iam_instance_profile        = data.aws_iam_instance_profile.EC2Role_profile.name
#   subnet_id                   = data.aws_subnet.int-subnet-03.id
#   associate_public_ip_address = true
#   key_name                    = "KhramovSA-SSH"
#   depends_on                  = [aws_db_instance.diploma_db]

#   user_data = data.template_cloudinit_config.cloudinit_cfg.rendered

#   # root_block_device {
#   #   volume_size = 20
#   # }

#   tags = {
#     Name  = "KhramovSA_Diploma_k8s_node-03"
#     owner = var.owner
#   }
#   volume_tags = {
#     Name  = "KhramovSA_Diploma_k8s_node-03"
#     owner = var.owner
#   }
# }

# resource "aws_instance" "diploma_k8s_master" {
#   ami                         = "ami-06ec8443c2a35b0ba"
#   instance_type               = "t2.micro"
#   vpc_security_group_ids      = [data.aws_security_group.diploma_sg_epam_ip.id]
#   iam_instance_profile        = data.aws_iam_instance_profile.EC2Role_profile.name
#   subnet_id                   = data.aws_subnet.int-subnet-01.id
#   associate_public_ip_address = true
#   key_name                    = "KhramovSA-SSH"
#   depends_on                  = [aws_db_instance.diploma_db]

#   user_data = data.template_cloudinit_config.cloudinit_cfg.rendered

#   # root_block_device {
#   #   volume_size = 20
#   # }

#   tags = {
#     Name  = "KhramovSA_Diploma_k8s_master"
#     owner = var.owner
#   }
#   volume_tags = {
#     Name  = "KhramovSA_Diploma_k8s_master"
#     owner = var.owner
#   }
# }

# ========== Instances security group ==============================
# ==================================================================
# data "aws_security_group" "diploma_sg_epam_ip" {
#   tags = {
#     "EPAM Cloud Default SG" = "true"
#     "or2-created-by"        = "orchestrator"
#   }
#   filter {
#     name   = "group-name"
#     values = ["Allow_conn_from_my_ip"]
#   }
# }


#==================================================================================================================================================================================================================
#==================================================================================================================================================================================================================
#========== Private EPAM account. =================================================================================================================================================================================
#==================================================================================================================================================================================================================
#==================================================================================================================================================================================================================

# # ========== Provider ==============================================
# # ==================================================================
# terraform {
#   required_providers {
#     aws = {
#       source  = "hashicorp/aws"
#       version = "~> 3.27"
#     }
#   }

#   required_version = ">= 0.14.9"
# }

# provider "aws" {
#   profile = "private_epam"

#   region  = "eu-central-1"

#   }

# data "aws_vpc" "default" {
#   default = true
# }

# data "aws_subnet" "int-subnet-01" {
#   filter {
#     name = "subnet-id"
#     values = ["subnet-07d1afac662ffe263"]
#   }
# }

# data "aws_subnet" "int-subnet-02" {
#   filter {
#     name = "subnet-id"
#     values = ["subnet-0ce856f877576de66"]
#   }
# }

# data "aws_subnet" "int-subnet-03" {
#   filter {
#     name = "subnet-id"
#     values = ["subnet-0515cf8d990d34dbf"]
#   }
# }

# resource "aws_db_subnet_group" "diploma_db_subnet_group" {
#   subnet_ids = [data.aws_subnet.int-subnet-01.id, data.aws_subnet.int-subnet-02.id, data.aws_subnet.int-subnet-03.id]

#   tags = {
#     owner = var.owner
#   }
# }

# # ========== ELB ===================================================
# # ==================================================================
# resource "aws_elb" "diploma_lb" {
#   name               = "diploma-lb"
#   internal = false
#   subnets = [data.aws_subnet.int-subnet-01.id, data.aws_subnet.int-subnet-02.id, data.aws_subnet.int-subnet-03.id]
#   security_groups = [data.aws_security_group.diploma_lb_sg.id]

#   listener {
#     instance_port     = 80
#     instance_protocol = "http"
#     lb_port           = 80
#     lb_protocol       = "http"
#   }

#   health_check {
#     healthy_threshold   = 5
#     unhealthy_threshold = 2
#     timeout             = 5
#     target              = "HTTP:80/readme.html"
#     interval            = 10

#   }

#   instances                   = [aws_instance.diploma_k8s_node-01.id, aws_instance.diploma_k8s_node-02.id, aws_instance.diploma_k8s_node-03.id]
#   cross_zone_load_balancing   = false
#   connection_draining         = true
#   connection_draining_timeout = 120

#   tags = {
#     Name = "KhramovSA_Diploma_lb"
#     owner = var.owner
#   }
# }

# # ========== ELB security group ====================================
# # ==================================================================
# data "aws_security_group" "diploma_lb_sg" {
#   filter {
#     name = "group-name"
#     values = ["Allow_conn_from_my_ip"]
#   }
# }

# # ========== EFS ===================================================
# # ==================================================================
# resource "aws_efs_file_system" "efs_for_dip" {

#   tags = {
#     Name = "KhramovSA_Diploma_efs_for_dip"
#     owner = var.owner
#     }
#   }

# resource "aws_efs_mount_target" "efs_mount_target_diploma_inst-01" {
#   file_system_id = aws_efs_file_system.efs_for_dip.id
#   subnet_id = data.aws_subnet.int-subnet-01.id
#   security_groups = [data.aws_security_group.diploma_efs_sg.id]
#   }

# resource "aws_efs_mount_target" "efs_mount_target_diploma_inst-02" {
#   file_system_id = aws_efs_file_system.efs_for_dip.id
#   subnet_id = data.aws_subnet.int-subnet-02.id
#   security_groups = [data.aws_security_group.diploma_efs_sg.id]
# }

# resource "aws_efs_mount_target" "efs_mount_target_diploma_inst-03" {
#   file_system_id = aws_efs_file_system.efs_for_dip.id
#   subnet_id = data.aws_subnet.int-subnet-03.id
#   security_groups = [data.aws_security_group.diploma_efs_sg.id]
# }

# # ========== EFS security group ==============================
# # ==================================================================
# data "aws_security_group" "diploma_efs_sg" {
#   filter {
#     name = "group-name"
#     values = ["Allow_conn_from_my_ip"]
#   }
# }

# # ========== DB instance ===========================================
# # ==================================================================
# resource "aws_db_instance" "diploma_db" {
#   identifier = "diploma-db"
#   engine = "mysql"
#   engine_version = "5.7.34"
#   allocated_storage = 20
#   instance_class = "db.t2.micro"
#   vpc_security_group_ids = [data.aws_security_group.diploma_db_sg.id]
#   availability_zone = "eu-central-1c"
#   db_subnet_group_name = aws_db_subnet_group.diploma_db_subnet_group.id
#   name = var.db_name
#   username = var.db_user
#   password = var.db_pass
#   parameter_group_name = "default.mysql5.7"
#   skip_final_snapshot = true
#   depends_on = [aws_efs_file_system.efs_for_dip]

#   tags = {
#     Name = "KhramovSA_Diploma_db"
#     owner = var.owner
#   }
# }

# # ========== DB security group ==============================
# # ==================================================================
# data "aws_security_group" "diploma_db_sg" {
#   filter {
#     name = "group-name"
#     values = ["Allow_conn_from_my_ip"]
#   }
# }

# # ========== Instances =============================================
# # ==================================================================

# data "template_file" "init_cfg" {
#   template = file("./cloud-init.yaml")
#   vars = {
#     "aws_efs_dns_name" = "${aws_efs_file_system.efs_for_dip.dns_name}"
#     "ansible_user_pass" = "${var.ansible_user_pass}"
#     "root_pass" = "${var.root_pass}"
#     "db_name" = "${var.db_name}"
#     "db_user" = "${var.db_user}"
#     "db_password" = "${var.db_pass}"
#     "db_host" = "${aws_db_instance.diploma_db.address}"
#     "my_ctrl_node_pub_key" = "${var.my_ctrl_node_pub_key}"
#     "my_virt_pc_pub_key" = "${var.my_virt_pc_pub_key}"
#   }
# }

# data "template_cloudinit_config" "cloudinit_cfg" {
#   gzip          = false
#   base64_encode = false

#   part {
#     filename     = "./cloud-init.yaml"
#     content_type = "text/cloud-config"
#     content      = data.template_file.init_cfg.rendered
#   }
# }

# resource "aws_instance" "diploma_k8s_node-01" {
#   ami = "ami-06ec8443c2a35b0ba"
#   instance_type = "t2.micro"
#   vpc_security_group_ids = [data.aws_security_group.diploma_sg_my_ip.id, data.aws_security_group.diploma_sg_epam_ip.id]
#   subnet_id = data.aws_subnet.int-subnet-01.id
#   associate_public_ip_address = true
#   key_name = "KhramovSA-private"
#   depends_on = [aws_db_instance.diploma_db]

#   user_data = data.template_cloudinit_config.cloudinit_cfg.rendered

#   tags = {
#     Name = "KhramovSA_Diploma_k8s_node-01"
#     owner = var.owner
#     }
#   volume_tags = {
#     Name = "KhramovSA_Diploma_k8s_node-03"
#     owner = var.owner
#     }
# }

# resource "aws_instance" "diploma_k8s_node-02" {
#   ami = "ami-06ec8443c2a35b0ba"  
#   instance_type = "t2.micro"
#   vpc_security_group_ids = [data.aws_security_group.diploma_sg_my_ip.id, data.aws_security_group.diploma_sg_epam_ip.id]
#   subnet_id = data.aws_subnet.int-subnet-02.id
#   associate_public_ip_address = true
#   key_name = "KhramovSA-private"
#   depends_on = [aws_db_instance.diploma_db]

#   user_data = data.template_cloudinit_config.cloudinit_cfg.rendered

#   tags = {
#     Name = "KhramovSA_Diploma_k8s_node-02"
#     owner = var.owner
#     }
#   volume_tags = {
#     Name = "KhramovSA_Diploma_k8s_node-03"
#     owner = var.owner
#     }
# }

# resource "aws_instance" "diploma_k8s_node-03" {
#   ami = "ami-06ec8443c2a35b0ba"
#   instance_type = "t2.micro"
#   vpc_security_group_ids = [data.aws_security_group.diploma_sg_my_ip.id, data.aws_security_group.diploma_sg_epam_ip.id]
#   subnet_id = data.aws_subnet.int-subnet-03.id
#   associate_public_ip_address = true
#   key_name = "KhramovSA-private"
#   depends_on = [aws_db_instance.diploma_db]

#   user_data = data.template_cloudinit_config.cloudinit_cfg.rendered

#   tags = {
#     Name = "KhramovSA_Diploma_k8s_node-03"
#     owner = var.owner
#     }
#   volume_tags = {
#     Name = "KhramovSA_Diploma_k8s_node-03"
#     owner = var.owner
#     }
# }

# resource "aws_instance" "diploma_k8s_master" {
#   ami = "ami-06ec8443c2a35b0ba"  
#   instance_type = "t2.micro"
#   vpc_security_group_ids = [data.aws_security_group.diploma_sg_my_ip.id, data.aws_security_group.diploma_sg_epam_ip.id]
#   subnet_id = data.aws_subnet.int-subnet-01.id
#   associate_public_ip_address = true
#   key_name = "KhramovSA-private"
#   depends_on = [aws_db_instance.diploma_db]

#   user_data = data.template_cloudinit_config.cloudinit_cfg.rendered

#   tags = {
#     Name = "KhramovSA_Diploma_k8s_master"
#     owner = var.owner
#     }
#   volume_tags = {
#     Name = "KhramovSA_Diploma_k8s_node-03"
#     owner = var.owner
#     }
# }

# # ========== Instances security group ==============================
# # ==================================================================
# data "aws_security_group" "diploma_sg_my_ip" {
#   filter {
#     name = "group-name"
#     values = ["Allow_conn_from_my_ip"]
#   }
# }

# data "aws_security_group" "diploma_sg_epam_ip" {
#   filter {
#     name = "group-name"
#     values = ["Allow_conn_from_EPAM"]
#   }
# }
