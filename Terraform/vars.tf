# variable "key_name" {
#   default = "KhramovSA-SSH"
#   # default = "KhramovSA-private"
# }

variable "aws_region" {
  default = "eu-central-1"
}

variable "db_name" {
  default = "diploma_db"
}

variable "eks_cl01_name" {
  default = "KhramovSA-Diploma-EKS-cl-01"
}

variable "eks_eksng01_name" {
  default = "KhramovSA-Diploma-k8s-eksng-01"
}

variable "owner" {
  default = "stepan_khramov@epam.com"
}

# variable "subnet-01_id" {
#   # EPAM corporate
#   default = "subnet-18068254"
#   # EPAM private
#   # default = "subnet-07d1afac662ffe263"
# }

# variable "subnet-02_id" {
#   # EPAM corporate  
#   default = "subnet-dc4a30b6"
#   # EPAM private
#   # default = "subnet-0ce856f877576de66"
# }

# variable "subnet-03_id" {
#   # EPAM corporate
#   default = "subnet-2965d455"
#   # EPAM private
#   # default = "subnet-0515cf8d990d34dbf"
# }

variable "root_pass" {
  type = string
}

variable "db_user" {
  type = string
}

variable "db_pass" {
  type = string
}

variable "my_ctrl_node_pub_key" {
  type = string  
}

variable "my_virt_pc_pub_key" {
  type = string  
}

variable "ansible_user_pass" {
  type = string  
}

variable "aws_av_zones" {
  type = string
  default = "eu-central-1a, eu-central-1b, eu-central-1c"
}